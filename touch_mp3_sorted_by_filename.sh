#!/bin/bash
################################################################################
# Title: Sort .mp3 files timestamp by filename.
# Description: Assigns a new sorted timestamp by the .mp3 filename
# Author: deliodc
################################################################################

# Code colors:
E_ERROR='\033[0;31m'
E_INFO='\e[0;34m'
E_WORK='\e[0;32m'
E_NC='\033[0m'

# Check input parameters
## Input directory:
if [ -z "$1" ]
 then
   ROOT="$(pwd .)"
   echo -e "${E_INFO}First parameter not defined using by default \"$ROOT${E_NC}\" as root directory.\n"
 else
   # TODO check if path is full or relative
   ROOT=$(basename "$1")
   ROOT=$(pwd .)"/"$ROOT
   echo -e "${E_INFO}First parameter using as root directory:\n\"$ROOT\"${E_NC}"
fi
## Output directory:
if [  -z "$2" ]
 then
   DATE_TITLE="$(date +%Y%m%d_%H%M%S)"
   OUTPUT_TITLE="OUTPUT_"$DATE_TITLE"_"$(basename "$ROOT")
   OUTPUT_DIR=$(pwd)"/"$OUTPUT_TITLE
   mkdir -vp "$OUTPUT_DIR"
   echo -e "${E_INFO}Second parameter not defined, using \"./$OUTPUT_TITLE\" as output directory:\n$OUTPUT_DIR${E_NC}\n"
 else
   OUTPUT_DIR="$2"
   mkdir -vp "$OUTPUT_DIR"
fi
# End check input parameters.

# Copies the files to output dir to avoid manipulate the original files, keeps
# the original attributes included datetime for preserver other files than mp3:
cp -vp "$ROOT"/* "$OUTPUT_DIR"

IFS=$'\n' # Only splits the for input when finds a line break
for FILE in $(ls "$OUTPUT_DIR"/*.mp3); # Only gets the .mp3 files
do
    touch "$FILE" # Updates the timestamp with touch binary
done



